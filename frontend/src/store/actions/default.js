import * as Action from './types'

const apiStatus = (api_status) => {
    return {
        type: Action.CHANGE_API_STATUS,
        payload: api_status
    }
}

export { apiStatus, }

