import * as Action from '../actions/types'

const appState = {
    city_list: [],
    modal: [],
    showloader: false,
    api_status: null,
    city_state_list: []
}

const defaultReducer = (state = appState, action) => {
    switch (action.type) {
        case Action.FETCH_CITYLIST_SUCCESS:
            return {
                ...state,
                city_list: action.payload.city_data
            };
        case Action.MODAL_MESSAGE:
            return {
                ...state,
                modal: [...state.modal, action.payload]
            }
        case Action.MODAL_DISMISS:
            return {
                ...state,
                modal: state.modal.filter(x => { if(x.key != action.payload.key){ return true}else{ if(x.dismissCallback){ x.dismissCallback() }; return false } })
            }
        case Action.MANIPULATE_LOADER:
            return {
                ...state,
                showloader: action.text
            }
        case Action.CHANGE_API_STATUS:
            return {
                ...state,
                api_status: action.payload
            }
        case Action.FETCH_CITYSTATELIST_SUCCESS:
            return {
                ...state,
                city_state_list: action.payload
            }
        default:
            return state;
    }
}

export default defaultReducer;