import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
// import { DEBUG, DEBUG_TOKEN } from "../../../local_config";
import { apiStatus } from './actions/default'
import * as status from '../utils/constants'

const rootReducer = combineReducers({

})

const actionHandler = store => next => action => {
  if (!action.hasOwnProperty("meta")) {
    return next(action)
  }
  if (action.meta.type == "API") {
    // // if (DEBUG) {
    //   action.meta.options.headers.append('Authorization', `Token ${DEBUG_TOKEN}`)
    // // }
    store.dispatch(apiStatus(status.ONGOING))
    fetch(action.meta.url, { ...action.meta.options }).then(resp => {
      if (resp.ok) {
        resp.json().then(data => {
          store.dispatch(action.meta.successAction(data, action.meta.extraParams));
        }).catch((error) => {
          throw error
        })
      } else {
        resp.json().then(data => {
          store.dispatch(action.meta.failureAction(data, action.meta.extraParams));
        }).catch((error) => {
          throw error
        })
      }
      setTimeout(() => store.dispatch(apiStatus(status.COMPLETED)), 500)
    }).catch((error) => {
      store.dispatch(apiStatus(status.COMPLETED))
      throw error
    })
  }
}

let middlewares = [thunk, actionHandler]
let composeEnhancers = compose

const Store = createStore(
  rootReducer,
  composeEnhancers(
    applyMiddleware(...middlewares))
);

export default Store;
