import React, { Component } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

import Admin from "../layouts/Admin.js";
import RTL from "../layouts/RTL.js";

export default class Router extends Component {
  render() {
    return (
      <Switch>
        <Route path="/admin" component={Admin} />
        <Route path="/rtl" component={RTL} />
        <Redirect from="/" to="/admin/dashboard" />
      </Switch>
    );
  }
}
