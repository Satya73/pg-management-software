export const ROOT_URL = "/salespro";
export const CMS_ROOT_URL = `${ROOT_URL}/cms`;
export const SUPPORT_ROOT_URL = `${ROOT_URL}/support`;
export const CAMPAIGN_URL = `${ROOT_URL}/campaign`;

export const menuLinkURL = {
    ROOT_URL: ROOT_URL,
    BASE_URL: `${ROOT_URL}/home`,
    SUPPORT: `${SUPPORT_ROOT_URL}`,
    CMS: `${CMS_ROOT_URL}`,
    CAMPAIGN_URL: `${CAMPAIGN_URL}`,
    ERROR_URL: `${ROOT_URL}/error`,
    ROOT_ERROR_URL: `${ROOT_URL}/error/invalid`
}
