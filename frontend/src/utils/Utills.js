import moment from 'moment'

const getCSRFtoken = () => {
    return (document.cookie.split(';').filter((item) => item.match('csrftoken'))[0] || "").split('=')[1]
}

const buildUrl = (url, parameters) => {
    let qs = "";
    for (const key in parameters) {
        if (parameters.hasOwnProperty(key)) {
            const value = parameters[key];
            qs += encodeURIComponent(key) + "=" + encodeURIComponent(value) + "&";
        }
    }
    if (qs.length > 0) {
        qs = qs.substring(0, qs.length - 1); //chop off last "&"
        url = url + "?" + qs;
    }
    return url;
}

const defaultResponseHandler = (type) => {
    return (data, extraParam) => ({
        type: type,
        payload: data,
        extraParam: extraParam
    })
}

const ResponseActions = (successType, failureType) => {
    return {
        onSuccess: defaultResponseHandler(successType),
        onFailure: defaultResponseHandler(failureType)
    }
}

const getQueryParams = (location) => {
    let query = new URLSearchParams(location.search);
    let params = {}
    query.forEach((val, key) => params[key] = val)
    return params
}

const isEmpty = (obj) => {
    return Object.keys(obj).length === 0 && obj.constructor === Object
}

const getDateFormat = (date, format = "DD MMM YYYY", input_format = "YYYY-MM-DD") => {
    if (date) {
        if (date instanceof Date) {
            return moment(date).format(format)
        } else if (date instanceof moment) {
            return date.format(format);
        } else {
            let abc = moment("2019-12-15 00:00:01", input_format).format(format)
            return moment(date, input_format).format(format)
        }
    }
    return ""
}

export {
    isEmpty,
    ResponseActions,
    buildUrl,
    getCSRFtoken,
    getQueryParams,
    getDateFormat,
}