export const ONGOING = "ONGOING";
export const COMPLETED = "COMPLETED";
export const FAILED = "FAILED";
export const SUCCESS = "SUCCESS";
export const INITIATED = "INITIATED";